package abstractFactory;

public class SamsungNote8 implements Phone {

    private String model;
    private String battery;
    private int wide;
    private int height;

    public SamsungNote8(String model, String battery, int wide, int height) {
        this.model = model;
        this.battery = battery;
        this.wide = wide;
        this.height = height;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public String getBattery() {
        return battery;
    }

    @Override
    public int getWide() {
        return wide;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "SamsungNote8{" +
                "model='" + model + '\'' +
                ", battery='" + battery + '\'' +
                ", wide=" + wide +
                ", height=" + height +
                '}';
    }
}
