package abstractFactory;

public class PhoneDealer {
    public static void main(String[] args) {
        SamsungS8Factory samsungS8Factory = new SamsungS8Factory();
        Phone s8 = samsungS8Factory.getPhone("s8", "2600mah", 4, 7);

        SamsungNote8Factory samsungNote8Factory = new SamsungNote8Factory();
        Phone note8 = samsungNote8Factory.getPhone("note8", "3000mah", 5, 8);

        System.out.println(s8);
        System.out.println(note8);


    }
}
