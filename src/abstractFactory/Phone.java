package abstractFactory;

public interface Phone {
    String getModel();

    String getBattery();

    int getWide();

    int getHeight();

}
