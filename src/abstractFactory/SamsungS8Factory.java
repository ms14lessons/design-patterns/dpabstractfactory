package abstractFactory;

public class SamsungS8Factory implements PhoneFactory{

    @Override
    public Phone getPhone(String model, String battery, int wide, int height) {
        return new SamsungS8(model, battery, wide, height);
    }
}
