package abstractFactory;

public class SamsungNote8Factory implements PhoneFactory{

    @Override
    public Phone getPhone(String model, String battery, int wide, int height) {
        return new SamsungNote8(model, battery, wide, height);
    }
}
